# Sequence Alignment

Sequence Alignment is one of the most important tasks in bioinformatics. It plays a fundamental role in sequence assembly, database searching, prediction of function, gene finding, and many other activities. It also facilitates the search for homologous genes, i. e., genes that come from a common ancestor and are not similar just by chance. By aligning sequences and searching for particularly conserved subsequences among homologous genes, one can find the regions of a protein that carry out important functions (and thus are less likely to be passed on to offspring in case of a mutation), these functional regions are called *protein domains*.

There are two types of sequence alignment: global and local. Global alignment finds the optimal alignment between two sequences given a cost function that rewards matches and penalizes mismatches and gaps. Finding the best among all possible arrangements would be an expensive task (it grows exponentially with respect to the sequence length), but a dynamic programming approach called the *Needleman-Wunsch algorithm* finds the optimal alignment with just O(nm) operations (where n and m are the lengths of the sequences). Local alignment is an even more daunting problem: the aim is to find the best possible alignment among all possible subsequences of the two sequences, that is, the best alignment allowing the removal of prefixes and suffixes to optimize the cost. This task, however, has its own dynamic programming solution: the *Smith-Waterman algorithm*. The optimal solution to both kinds of alignments depends significantly on the scoring function, and the complexity of this function will in turn depend on the problem at hand. Once an optimal alignment has been found, a proper hypothesis testing must take place in order to determine that a pair of sequences corresponds to homologous genes and is not similar by pure chance.

Examples:

* Global alignment:

```
VIVALASVEGAS
|||| | |   |
VIVADA-V--IS
```

* Local alignment:

```
QUEVIVALASVEGAS
RRR||||RRRRRRRR --> R here means "removed"
---VIVADA-V--IS
```

For situations in which even the DP approaches are too expensive (such as when searching huge databases for similar sequences), heuristic techniques can be applied to find similar sequences. These techniques don't guarantee that the optimal alignment will be found, but they find a *good enough* alignment that will help identify similar sequences with lesser computational cost. One such heuristic algorithm is BLAST, one of the most used sequence searching algorithms.

Multiple sequence alignment poses yet another challenge: the Needleman-Wunsch and Smith-Waterman algorithms can be extended to multiple sequences, but their cost grows exponentially with the number of sequences being compared, which is not very convenient when dealing with large datasets. Heuristic algorithms are commonly used in practice when it comes to multiple alignment, with *CLUSTALW* being one of the most popular choices,
