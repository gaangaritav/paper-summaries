# An Introduction to Quantum Computing
(Una Introducción a la Computación Cuántica)

[DOI 10.1007/978-94-007-0080-2_10](https://www.researchgate.net/publication/1758552_An_Introduction_to_Quantum_Computing)

En un sistema físico clásico (determinístico) cada objeto está en un único estado a la vez (por ejemplo, solo puede estar en una ubicación del espacio en cada momento). Por otra parte, en el mundo cuántico cada objeto se encuentra simultáneamente en varios estados, y solo *decide* tomar un estado determinado una vez es observado (ver el [experimento de Young](https://es.wikipedia.org/wiki/Experimento_de_Young) llevado a cabo con electrones).

Los computadores tradicionales utilizan bits que funcionan según las reglas de la física clásica: un bit se encuentra en el estado 1 o en el estado 0 en un momento dado, pero nunca en los dos estados a la vez. Los computadores cuánticos utilizan cubits: partículas subatómicas que pueden estar en diferentes estados (órbitas, estados de polarización, dirección del spin, etc.), y que se rigen por las leyes de la mecánica cuántica: es decir, se encuentran simultáneamente en los dos posibles estados hasta el momento en que son observados.

Un bit tradicional o un cubit en un estado "puro" (no superpuesto) se puede representar como un vector columna:

```
|1⟩ = [0, 1]^T

|0⟩ = [1, 0]^T
```

y las transformaciones o funciones sobre estos cubits se pueden representar como matrices, por ejemplo:

```
NOT(1) = 0:

⎡0 1⎤⎡0⎤ ⎽ ⎡1⎤
⎣1 0⎦⎣1⎦ ⎺ ⎣0⎦
```

de forma similar, un cubit que se encuentra en una superposición de estados se puede representar como un vector en el que cada entrada representa la probabilidad de encontrarse en ese estado al momento de ser *observado*:

|ɸ⟩ = a|0⟩ + b|1⟩ = [a, b]^T

La combinación de dos sistemas cuánticos se puede representar como el producto tensorial entre los dos bits individuales:

|1⟩ ⊗ |0⟩ = |10⟩ = [0[1, 0], 1[1, 0]]^T = [0, 0, 1, 0]^T

Al tratarse de cubits no puros, el resultado de este producto adquiere un nuevo significado, ya que no puede ser visto como dos bits independientes; la combinación no se pueden representar simplemente como la unión de dos estados independientes. En este caso se dice que los estados están entrelazados:

(a|0⟩ + b|1⟩) ⊗ (c|0⟩ + d|1⟩) = [a[c, d], b[c, d]]^T = [ac, ad, bc, bd]^T

Al encontrarse en estado de superposición, es posible operar simultáneamente sobre los dos posibles estados, realizando así operaciones en paralelo de una manera que es imposible en un computador tradicional.

Un ejemplo clásico es el llamado Algoritmo de Deutsch. En él, se tiene una caja negra con una función unaria que puede ser "constante" (si retorna siempre el mismo resultado) o "balanceada" (si retorna resultados diferentes para cada entrada). Se busca determinar si la función de la caja negra es constante o balanceada.

En un sistema tradicional, se necesitaría evaluar dos veces la función: una para el bit 0 y otra para el 1, y luego comparar los resultados; si son iguales es constante y si son diferentes es balanceada. En un sistema cuántico es posible evaluar una única vez la función sobre un cubit que se encuentra en superposición entre los estados 0 y 1.
